<?php get_header(); ?>
<div class="main">


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> <!-- start loop -->

			<article <?php post_class(); ?>>

				<?php if(!is_front_page()) : ?><h1 class="entry-title"><?php the_title(); ?></h1><?php endif; ?>

				<?php if(have_rows('slides')) :  ?>
				<div class="slider-wrapper js-flickity" data-flickity-options='{ "cellAlign": "center", "percentPosition": true, "imagesLoaded" : true }'>
					<?php while(have_rows('slides')) : the_row(); ?>
					<?php $img = get_sub_field('slide'); ?>
						<div class="slide">
							<img src="<?php echo $img['sizes']['homeslider']; ?>" alt="">
						</div>
					<?php endwhile; ?>
				</div>

				<?php elseif(has_post_thumbnail()) : the_post_thumbnail('large'); endif; ?>
				<div class="entry-content inquire-page">
					<div class="content-column one_half">
						<?php if( get_query_var( 'artwork')): ?>
							<?php $cheese = get_query_var('artwork'); ?>
							<?php $image_size = apply_filters( 'wporg_attachment_size', 'large' ); 
             echo wp_get_attachment_image( $cheese, $image_size ); ?>
             			<span class="artist-slide-caption"><?php echo get_the_excerpt($cheese); ?></span>
						<?php else: ?>
						
					
						<?php endif; ?>
					</div>
					<div class="content-column one_half last_column">
						<?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
					</div>
				</div>
			</article>

<?php endwhile; endif; ?> <!-- end loop -->
		
</div>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>