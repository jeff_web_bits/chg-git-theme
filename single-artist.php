<?php get_header(); ?>

<div class="main">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> <!-- start loop -->

<?php $args = array(
			'post_type' => 'post',
			'meta_query' => array(
				array(
					'key' => 'related_artists',
					'value' => $post->ID,
					'compare' => 'LIKE'
					)
				)
		); 
		 $press = get_posts($args); ?>


<div class="sub-artist-menu">
<h1 class="entry-title"><?php the_title(); ?></h1>
<div>
	<a href="#biography">Biography</a>

		<?php if($press) : ?>
		<a href="#artistnews">Press</a>
	<?php endif; ?>

	<?php if (get_field('artist_publications')): ?>
	<a href="#publications">Publications</a>
	<?php endif; ?>

	<?php if(get_field('artist_videos')): ?>
	<a href="#videos">Videos</a>
	<?php endif; ?>


</div>
<a href="#top" id="totop"><i class="fa fa-2x fa-angle-up"></i></a>
</div>


<div class="artist-bio">
	<?php // the_content(); ?>
	<?php // ACF Image Gallery
	$images = get_field( 'artist_gallery' );

	if($images) : ?>

		<div class="artist-grid">
			<?php $i = 0; ?>
			<?php foreach($images as $image) : ?>
				<?php $i++; ?>
				<div  class="artist-in-grid"><img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['caption']; ?>"></div>
			<?php endforeach; ?>
		</div>

	<?php

	endif;



	if ( $images ) : ?>

		<?php $i = 0; ?>

		<div class="artist-slider-yay">
		
		<div id="yay-close"></div>

	    <?php foreach ( $images as $image ) : ?>

	    	<?php $i++; ?>

	    	<div class="artist-slide">
			
			<div class="artist-slide-art" style="background-image:url(<?php echo $image['sizes']['artwork']; ?>);">
	    		
	    	</div>

	    	<div class="artist-slide-caption sans"><?php echo $image['caption']; ?></div>
	      
			<a href="/inquire?artwork=<?php echo $image['ID']; ?>&title=<?php echo $image['caption']; ?>" class="inquire-button">Inquire</a>

			</div>

	    <?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>

<div class="artist-bio has-anchor">
	<div class="the-anchor" id="biography">&nbsp;</div>
	<h2><?php the_title(); ?> Biography</h2>
	<?php the_field('artist_biography'); ?>
</div>

	<?php 
		
		 if($press) :?>
<div class="artist-news has-anchor" >
	<div id="artistnews" class="the-anchor">&nbsp;</div>
	<h2>Press</h2>
		 	<?php
		 foreach($press as $post) : setup_postdata($post); ?>
		 
			<div class="artist-news-item">
				<?php if(get_field('publication_date')) : ?><div class="artist-news-date"><?php the_field('publication_date'); ?></div><?php endif; ?>
				<?php if(get_field('publication_name')) : ?><div class="artist-news-name"><strong><?php the_field('publication_name'); ?></strong></div><?php endif; ?>
				<?php if(get_field('publication_byline')) : ?><div class="artist-news-byline"><?php the_field('publication_byline'); ?></div><?php endif; ?>
				<?php if(get_field('original_url')) : ?><a href="<?php the_field('original_url'); ?>" target="_blank" class="artist-original-link">Link to Article</a><?php endif; ?>

				<h3><?php the_title(); ?></h3>
				  <?php the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>" class="readmore sans"><strong>Read More</strong></a>
			</div>

		 <?php
		 endforeach;wp_reset_postdata();?>
		</div>
		 <?php
		 endif;

	?>

<?php if(get_field('artist_publications')): ?>
<div class="artist-news has-anchor sans" >
	<div id="publications" class="the-anchor">&nbsp;</div>
	<h2>Publications</h2>
	<?php the_field('artist_publications') ?>
</div>
<?php endif; ?>

<?php if(get_field('artist_videos')): ?>
<div class="artist-bio has-anchor" >
	<div id="videos" class="the-anchor">&nbsp;</div>
	<h2>Videos</h2>
	<?php the_field('artist_videos') ?>
</div>
<?php endif; ?>

<?php endwhile; endif; ?> <!-- end loop -->

</div>	

<?php // get_sidebar(); ?>

<?php get_footer(); ?>