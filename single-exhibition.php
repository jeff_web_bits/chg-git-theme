<?php get_header(); ?>
<div class="main">


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> <!-- start loop -->

			<article <?php post_class(); ?>>

				

				<?php if(have_rows('slides')) :  ?>
				<div class="slider-wrapper js-flickity" data-flickity-options='{ "cellAlign": "center", "percentPosition": true, "imagesLoaded" : true }'>
					<?php while(have_rows('slides')) : the_row(); ?>
					<?php $img = get_sub_field('slide'); ?>
						<div class="slide">
							<img src="<?php echo $img['sizes']['homeslider']; ?>" alt="">
						</div>
					<?php endwhile; ?>
				</div>

				<?php elseif(has_post_thumbnail()) : ?>
					<div class="featured-image-with-caption">
					<?php the_post_thumbnail('large'); 
					the_post_thumbnail_caption(); ?>
				</div>
					<?php
				endif; ?>
				<div class="entry-content <?php if(is_page('contact')): ?>sans <?php endif; ?>">

					<?php if(!is_front_page()) : ?><h1 class="entry-title">
						
						

						<?php the_title(); ?></h1><?php endif; ?>
					<?php  the_content(); ?>

					<div class="artist-bio">
	<?php // the_content(); ?>
	<?php // ACF Image Gallery
	$images = get_field( 'exhbition_the_gallery' );

	if($images) : ?>

		<div class="artist-grid">
			<?php $i = 0; ?>
			<?php foreach($images as $image) : ?>
				<?php $i++; ?>
				<a href="#image<?php echo $i ?>" class="artist-in-grid"><img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['caption']; ?>"></a>
			<?php endforeach; ?>
		</div>

	<?php

	endif;



	if ( $images ) : ?>

		<?php $i = 0; ?>

		<div class="artist-slider-yay">
		
		<div id="yay-close"></div>

	    <?php foreach ( $images as $image ) : ?>

	    	<?php $i++; ?>

	    	<div class="artist-slide" id="image<?php echo $i; ?>">
			
			<div class="artist-slide-art" style="background-image:url(<?php echo $image['sizes']['artwork']; ?>);">
	    		
	    	</div>

	    	<div class="artist-slide-caption sans"><?php echo $image['caption']; ?></div>
	      
			<a href="/inquire?artwork=<?php echo $image['ID']; ?>&title=<?php echo $image['caption']; ?>" class="inquire-button">Inquire</a>

			</div>

	    <?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>



				</div>
			</article>

<?php endwhile; endif; ?> <!-- end loop -->
		
</div>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>