<?php get_header(); ?>
<div class="main">


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> <!-- start loop -->

			<article <?php post_class(); ?>>

				<?php if(have_rows('slides')) :  ?>
				<div class="slider-wrapper mobile-hide js-flickity" data-flickity-options='{ "cellAlign": "center", "percentPosition": true, "imagesLoaded" : true, "pageDots" : true, "prevNextButtons" : false}'>
					<?php while(have_rows('slides')) : the_row(); ?>
					<?php $img = get_sub_field('slide'); ?>
						<div class="slide">
							<img src="<?php echo $img['sizes']['homeslider_large']; ?>" alt="">
						</div>
					<?php endwhile; ?>
				</div>
				

				<div class="slider-wrapper mobile-show">

				<?php // ACF Image Object
				$image     = get_field( 'mobile_home' );
				$alt       = $image['alt'];
				$imageSize = $image['sizes'][ 'large' ];
				
				echo '<img class="" src="' . $imageSize . '" alt="' . $alt . '" />';
				 ?>

				</div>

				<?php elseif(has_post_thumbnail()) : the_post_thumbnail('large'); endif; ?>
				<div class="entry-content homepage">
					<h1 class="homepage-title screen-reader-text">Courthouse Gallery Fine Art</h1>

					<?php the_content(); ?>


					<?php $exes = get_field('homepage_content');

					if($exes) :?>
						<div class="exhibition-grid homepage">
						<?php

						foreach($exes as $post) : setup_postdata($post);
						?>

						<div class="exhibition-item">
							<?php if(get_field('more_info') || get_field('artist_biography')): ?><a href="<?php the_permalink(); ?>"><?php endif; ?>
							<?php the_post_thumbnail('exhibition'); ?>
								<?php if(get_field('more_info')): ?></a><?php endif; ?>

							<strong class=exhibition-item-title>
							<?php the_title(); ?></strong>							
							<?php the_field('show_details'); ?>
							<?php if(get_field('more_info') || get_field('artist_biography')): ?><br>
							<a href="<?php the_permalink(); ?>" class="bold"><?php if (get_field('more_info_text')): the_field( 'more_info_text' ); else: ?>More Info<?php endif;?></a>
							<?php endif; ?>
							
						</div>

						<?php
						endforeach;wp_reset_postdata();	?>

						</div>

						<?php

					endif;


					 ?>


				</div>
			</article>

<?php endwhile; endif; ?> <!-- end loop -->
		
</div>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>