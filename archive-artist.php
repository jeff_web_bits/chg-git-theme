<?php get_header(); ?>
<div class="main">
	<h1 class="entry-title">Artists</h1>
<div class="artist-grid">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> <!-- start loop -->

<a class="artist-in-grid" href="<?php the_permalink();?>">
<?php the_post_thumbnail('artistgrid');?>
<h2><?php the_title();?></h2>
</a>


<?php endwhile; endif; ?> <!-- end loop -->
</div>
</div>
<?php //  get_sidebar(); ?>

<?php get_footer(); ?>