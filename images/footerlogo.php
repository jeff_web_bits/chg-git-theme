<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 161.67 47.67"><defs><style>.cls-1{fill:#231f20;}</style></defs><title>CEG</title><path class="cls-1" d="M95.95,9H59.14A4.06,4.06,0,0,1,55,4.94,4.06,4.06,0,0,1,59.14.87H95.95A4.06,4.06,0,0,1,100,4.94,4.06,4.06,0,0,1,95.95,9Zm0,18.47H59.14a4.09,4.09,0,0,1-2.9-7,4,4,0,0,1,2.9-1.19H95.95a4.08,4.08,0,1,1,0,8.16Zm0,18.37H59.14a4.08,4.08,0,1,1,0-8.16H95.95a4.08,4.08,0,1,1,0,8.16Z"/><path class="cls-1" d="M41.85,45.87H5a4,4,0,0,1-2.9-1.19A4,4,0,0,1,1,41.77V5a4,4,0,0,1,1.19-2.9A4,4,0,0,1,5,.87H41.85a4,4,0,0,1,2.9,1.19A3.92,3.92,0,0,1,46,4.94a3.92,3.92,0,0,1-1.19,2.89A4,4,0,0,1,41.85,9H9.11V37.71H41.85a4,4,0,0,1,2.91,1.19,4.1,4.1,0,0,1,0,5.78A4,4,0,0,1,41.85,45.87Z"/><path class="cls-1" d="M154.55,45.87H117.74a4.07,4.07,0,0,1-4.1-4.1V5a3.94,3.94,0,0,1,1.19-2.9,4,4,0,0,1,2.9-1.19h36.81a4,4,0,0,1,2.9,1.19,3.93,3.93,0,0,1,1.19,2.89,3.93,3.93,0,0,1-1.19,2.89A4,4,0,0,1,154.55,9H121.8V37.71h28.69V27.4H136.15a4,4,0,0,1-2.91-1.19,4.1,4.1,0,0,1,0-5.78,4,4,0,0,1,2.9-1.19h18.4a4.07,4.07,0,0,1,4.1,4.1V41.77a4.07,4.07,0,0,1-4.1,4.1Z"/></svg>