<?php get_header(); 

// Template Name: Exhibitions Page

?>
<div class="main">


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> <!-- start loop -->

			<article <?php post_class(); ?>>

					<div class="entry-title-flex">
						<h1 class="entry-title"><?php the_title(); ?></h1>
						<?php if(get_field('show_past_exhibitions')): ?>
							<!-- <a href="#past">Past Exhibits</a> -->
						<?php endif; ?>
					</div>
					
					<?php $exes = get_field('exhibitions_to_show');

					if($exes) :?>
						<div class="exhibition-grid">
						<?php

						foreach($exes as $post) : setup_postdata($post);
						?>

						<div class="exhibition-item">
							<?php if(get_field('more_info') || get_field('artist_biography')): ?><a href="<?php the_permalink(); ?>"><?php endif; ?>
							<?php the_post_thumbnail('exhibition'); ?>
								<?php if(get_field('more_info')): ?></a><?php endif; ?>

							<strong class=exhibition-item-title>
								<?php if(get_field('pre_title')): ?><div class="pre-title"><?php the_field( 'pre_title' ); ?></div><?php endif; ?>
								<?php the_title(); ?></strong>
							<?php the_field('show_details'); ?>
							<?php if(get_field('more_info') || get_field('artist_biography')): ?><br>
							<a href="<?php the_permalink(); ?>" class="bold"><?php if (get_field('more_info_text')): the_field( 'more_info_text' ); else: ?>More Info<?php endif;?></a>
							<?php endif; ?>
						</div>

						<?php
						endforeach;	wp_reset_postdata();?>

						</div>

						<?php

					endif;


					 ?>

<?php if(get_field('show_past_exhibitions')): ?>
<div id="past"></div>
<?php 

if ( have_rows( 'past_exhibitions', 'options' ) ) : ?>
	
		<?php while ( have_rows( 'past_exhibitions', 'options' ) ) : the_row(); ?>
			<h2 class="entry-title"><?php the_sub_field( 'year' ); ?></h2>
			<div class="exhibition-grid past">
				<?php $exes = get_sub_field('exhibitions'); ?>
				<?php
						if($exes):
						foreach($exes as $post) : setup_postdata($post);
						?>

						<div class="exhibition-item">
							<?php if(get_field('more_info')): ?><a href="<?php the_permalink(); ?>"><?php endif; ?>
							<?php the_post_thumbnail('exhibition'); ?>
								<?php if(get_field('more_info')): ?></a><?php endif; ?>

							<strong class=exhibition-item-title>
								<?php if(get_field('pre_title')): ?><div class="pre-title"><?php the_field( 'pre_title' ); ?></div><?php endif; ?>
								<?php the_title(); ?></strong>
							<?php the_field('show_details'); ?>
							<?php if(get_field('more_info')): ?><br>
							<a href="<?php the_permalink(); ?>" class="bold"><?php if (get_field('more_info_text')): the_field( 'more_info_text' ); else: ?>More Info<?php endif;?></a>
							<?php endif; ?>
						</div>

						<?php
						endforeach;wp_reset_postdata();endif;	?>
			</div>
		<?php endwhile; ?>
	
<?php endif; ?>

<?php endif; ?>


<!--
<br><br><br><h2 class="entry-title">Past Exhibitions</h2>
					 <?php $exes = get_field('past_exhibitions_to_show');

					if($exes) :?>
						<div class="exhibition-grid past">
						<?php

						foreach($exes as $post) : setup_postdata($post);
						?>

						<div class="exhibition-item">
							<?php the_post_thumbnail('homeslider'); ?>

							<strong><?php the_title(); ?></strong><br>
							<?php the_field('show_details'); ?>
							<?php if(get_field('more_info')): ?><br>
							<a href="<?php the_permalink(); ?>">More Info</a>
							<?php endif; ?>
						</div>

						<?php
						endforeach;wp_reset_postdata();	?>

						</div>

						<?php

					endif;


					 ?>

					-->


			</article>

<?php endwhile; endif; ?> <!-- end loop -->
		
</div>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>