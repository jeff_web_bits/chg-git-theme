<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0">
	<title><?php wp_title( ' | ', true, 'right' ); ?></title>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://use.typekit.net/iys7edy.css">
	<link rel="stylesheet" href="https://use.typekit.net/nbn7dup.css">

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#86cdde">
<meta name="msapplication-TileColor" content="#86cdde">
<meta name="theme-color" content="#ffffff">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#ff8a01">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-4209452-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-4209452-1');
</script>


</head>
<body <?php body_class(); ?>>
	
	<div class="header-wrapper">
		<div class="header-inner">
			<div class="logo">
				<a href="/"><?php get_template_part('images/chg') ?></a>
			</div>
			<div id="menu-toggle"></div>
			<div class="navigation-inner">
			<?php wp_nav_menu(); ?>
		</div>
		</div>
	</div>
	<div class="navigation-wrapper">
		
	</div>


<div class="content-wrapper">

	