<aside class="sidebar">
	<h3 class="sidebar-menu-title">Contemporary Artists <button>Toggle Artists</button></h3>
	<div class="sidebar-navigation-wrap">
	<nav class="sidebar-navigation">
	<?php 
		$contemporary = get_field('contemporary_artists', 'options');

		foreach($contemporary as $post) : setup_postdata($post);
	?>
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	<?php 
		endforeach;wp_reset_postdata();
	 ?>
	</nav>
	</div>
</aside>

