<?php get_header(); ?>
<div class="main presspage">
	<h1 class="entry-title">News & Press</h1>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> <!-- start loop -->
			
			<article <?php post_class("presspage-article"); ?>>
				<div class="leftside">
					<?php the_post_thumbnail('medium'); ?>
					
				</div>
				<div class="rightside">
					<div class="sans">
				<?php if(get_field('publication_date')): ?><?php the_field('publication_date'); ?><br><?php endif; ?>
				<?php if(get_field('publication_name')): ?><?php the_field('publication_name'); ?><br><?php endif; ?>
				<?php if(get_field('publication_byline')): ?><?php the_field('publication_byline'); ?><br><?php endif; ?>
				<?php if(get_field('original_url')) : ?><a href="<?php the_field('original_url') ?>" target="_blank" class="sans">Link to Article</a><?php endif; ?>
				</div>
				<h2 class="entry-title sans bold"><?php the_title(); ?></h2>
				<div class="entry-content "><?php the_excerpt(); ?></div>
				<a class="readmore sans bold" href="<?php the_permalink(); ?>">Read More</a>
				</div>
			</article>

<?php endwhile; 

the_posts_pagination( array(
					'prev_text'          => __( '<<', 'swmcwp' ),
					'next_text'          => __( '>>', 'swmcwp' ),
				) );

endif; ?> <!-- end loop -->
		
</div>
<?php  // get_sidebar(); ?>

<?php get_footer(); ?>