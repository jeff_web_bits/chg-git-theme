<?php get_header(); ?>
<div class="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> <!-- start loop -->

			<article <?php post_class(); ?>>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<div class="entry-content "><?php the_content(); ?></div>
			</article>

<?php endwhile; endif; ?> <!-- end loop -->
		
</div>
<?php //  get_sidebar(); ?>

<?php get_footer(); ?>