jQuery(document).ready(function($){

	$('.collapser').each(function(){
		var origHeight = $(this).height();
		$(this).height(100);
		var toggler = true;
		$(this).next('.slideauto').on('click', function(e){
			e.preventDefault();
			if(toggler) {
				$(this).html('Collapse');
				$(this).prev('.collapser').height(origHeight);
				toggler = false;
			} else {
				$(this).prev('.collapser').height(100);
				toggler = true;
				$(this).html('Read More');
			}
		});

	});

	$('.sidebar-menu-title button').on("click", function(){
		$('.sidebar-navigation-wrap').slideToggle();
	});

	$('#menu-toggle').on('click', function(e){
		e.preventDefault();
		$('.menu-main-menu-container .menu ').slideToggle();
	});






	$('#yay-close').on('click', function(){
		$('.artist-slider-yay').addClass('goAway');
		// $('html').removeClass('noScroll');
	});

		$(document).keyup(function(e) {
		if(e.keyCode === 27) {
			$('.artist-slider-yay').addClass('goAway');
		}
	});



	  $artistSlider = $('.artist-slider-yay').flickity({
	  	cellAlign : "center",
	  	pageDots : false,
	  	percentPosition : true,
	  	imagesLoads : true,
	  	cellSelector : '.artist-slide',
	  }).addClass('goAway');

	$('.single-artist .artist-in-grid, .single-exhibition .artist-in-grid').on('click', function(){
		$('.artist-slider-yay').removeClass('goAway');
		// e.preventDefault();
		// $('html').addClass('noScroll');
		var index = $(this).index();
		$artistSlider.flickity( 'select', index, false, true);

	});

	$artistSlider.on( 'dragStart.flickity', function( event, pointer ) {
    document.ontouchmove = function (e) {
        e.preventDefault();
    }
});
$artistSlider.on( 'dragEnd.flickity', function( event, pointer ) {
    document.ontouchmove = function (e) {
        return true;
    }
});






});