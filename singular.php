<?php get_header(); ?>
<div class="main">


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> <!-- start loop -->

			<article <?php post_class(); ?>>

				

				<?php if(have_rows('slides')) :  ?>
				<div class="slider-wrapper js-flickity" data-flickity-options='{ "cellAlign": "center", "percentPosition": true, "imagesLoaded" : true }'>
					<?php while(have_rows('slides')) : the_row(); ?>
					<?php $img = get_sub_field('slide'); ?>
						<div class="slide">
							<img src="<?php echo $img['sizes']['homeslider']; ?>" alt="">
						</div>
					<?php endwhile; ?>
				</div>

				<?php elseif(has_post_thumbnail()) : ?>
					<div class="featured-image-with-caption">
					<?php the_post_thumbnail('large'); 
					the_post_thumbnail_caption(); ?>
				</div>
					<?php
				endif; ?>
				<div class="entry-content <?php if(is_page('contact')): ?>sans <?php endif; ?>">
					<?php if(!is_front_page()) : ?><h1 class="entry-title">
						
						<?php if(get_field('pre_title')): ?><div class="pre-title"><?php the_field( 'pre_title' ); ?></div><?php endif; ?>

						<?php the_title(); ?></h1><?php endif; ?>
					<?php the_content(); ?>
				</div>
			</article>

<?php endwhile; endif; ?> <!-- end loop -->
		
</div>

<?php // get_sidebar(); ?>

<?php get_footer(); ?>