<?php 

// ENABLE FEATURED IMAGE

add_theme_support( 'post-thumbnails' ); 
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'title-tag' ) );


// REGISTER MENU LOCATION

register_nav_menus(array(
	'primary_navigation' => 'Primary Navigation'
	));


function enqueue_all_the_things() {
	wp_enqueue_style( 'main-styles', get_stylesheet_directory_uri() . '/style.css?r=' . rand(100000, 999999) );
  wp_enqueue_style( 'flickity', get_stylesheet_directory_uri() . '/_js/flickity.css' );
	wp_enqueue_style( 'oops', get_stylesheet_directory_uri() . '/oops.css' );
  wp_enqueue_script( 'flickity', get_template_directory_uri() . '/_js/flickity.js', array('jquery', 'chgjs'));
  wp_enqueue_script( 'chgjs', get_template_directory_uri() . '/_js/chg.js', array('jquery'));
}

add_action( 'wp_enqueue_scripts', 'enqueue_all_the_things' );


// custom image sizes
 
add_action( 'after_setup_theme', 'swmc_theme_setup' );
function swmc_theme_setup() {
   add_image_size( 'artwork',  950, 750);
   add_image_size( 'rollover', 260, 250, true);
   add_image_size('homeslider', 650, 350, true);
   add_image_size('homeslider_large', 1300, 700, true);
   add_image_size('artistgrid', 246, 180, true);
   add_image_size('exhibition', 400, 300, true);
}

// options page

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' => 'Site Options',
		'menu_title' => 'Site Options',
		'position' => '31.1'
		));


  acf_add_options_page(array(
    'page_title' => 'Past Exhibitions',
    'menu_title' => 'Past Exhibitions',
    'parent_slug' => 'edit.php?post_type=exhibition'
    ));


	acf_add_options_page(array(
		'page_title' => 'Choose Default Sidebar',
		'menu_title' => 'Default Sidebar',
		'parent_slug' => 'edit.php?post_type=sidebar'
		));
}

// FIX wp_title

add_filter( 'wp_title', 'swmcwp_filter_wp_title' );
function swmcwp_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}


//remove inline width and height added to images

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   // $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}


function wpmark_alter_team_archive_template_query( $query ) {
    
    /* only proceed on the front end */
    if( is_admin() ) {
	    return;
    }
    
    /* only on the person post archive for the main query */
    if ( $query->is_post_type_archive( 'artist' ) && $query->is_main_query() ) {
	    
        $query->set( 'posts_per_page', -1 );
        $query->set( 'orderby', 'name');
        $query->set( 'order', 'ASC');
        
    }
    
}
add_action( 'pre_get_posts', 'wpmark_alter_team_archive_template_query' );



function add_query_vars_filter( $vars ) {
  $vars[] = "artwork";
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );




// here we go

add_filter('edit_attachment', 'matchy_matchy');

function matchy_matchy( $post_id ) {

if(get_post_type() == 'attachment') {
  $caption = get_the_excerpt($post_id);
    update_post_meta( $post_id, '_wp_attachment_image_alt', $caption );
  }
}
?>