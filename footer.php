</div> <!-- content-wrapper -->
	
<a href="#top" class="totop"><i class="fa fa-angle-up"></i></a>

<div class="footer-wrapper">
	<div class="footer-inner">
		
		

		<div class="footer-logo">
			<?php get_template_part('images/footerlogo') ?>
		</div>
		
		&copy; Courthouse Gallery Fine Art 
			<span class="sep"></span>
		6 Court Street, Ellsworth, Maine 04605
			<span class="sep"></span>
		 <span class="phone">207 667 6611</span>
		 <div class="footer-social">
		 	<a href="https://www.facebook.com/courthousegallery/" targe="_blank" class="social"><i class="fa fa-facebook"></i></a>
		<a href="https://www.instagram.com/courthousegallery" targe="_blank" class="social"><i class="fa fa-instagram"></i></a>
		<a href="/contact" targe="_blank" class="social"><i class="fa fa-envelope-o"></i></a>
		 </div>
	</div>
	<div class="footer-inner noline">
		<a href="https://courthousegallery.us7.list-manage.com/subscribe/post?u=f57327fce9cbbced2ba772db7&amp;id=5a61313960" target="_blank" class="joinus"><i class="fa fa-envelope"></i> Join Our Email List</a>
	</div>
</div>
	<?php wp_footer(); ?>

</body>
</html>